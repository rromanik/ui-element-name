package nik.roma.elementname;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import nik.roma.elementname.pages.GooglePage;

public class HtmlElementNameTest {
	private WebDriver driver;

	@Test
	public void setup() {
		WebDriver driver = new HtmlUnitDriver();
		GooglePage googlePage = new GooglePage(driver);
		googlePage.searchFor("appium java-client");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		if (driver != null)
			driver.quit();
		driver = null;
	}

}
