package nik.roma.elementname.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public class GooglePage {

	@Name("'Search' Field")
	@FindBy(name = "q")
	private WebElement searchField;

	public GooglePage(WebDriver driver) {
		driver.get("https://www.google.com");
		// HtmlElementLoader.populate(this, driver);
		//PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
		PageFactory.initElements(driver, this);
	}

	public void searchFor(String query) {
		System.out.println("Sends keys into " + searchField);
		searchField.sendKeys(query);
		searchField.submit();
	}
}
